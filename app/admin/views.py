from flask import *
from . import admin
from .. import db
from .forms import LoginForm
from ..models import *
from flask_login import login_user

@admin.route('/login', methods=['GET', 'POST']) 
def userlogin():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data) 
            next = request.args.get('next')
            if next is None or not next.startswith('/'):
                next = url_for('mairie.accueil') 
            return redirect(next)
        flash('Invalid username or password.')
    return render_template('admin/userlogin.html',form=form )

@admin.route('/admin/login') 
def adminlogin():
    return render_template('admin/adminlogin.html')

@admin.route('/admin/dashboard') 
def presentation():
    return render_template('admin/presentation.html')

@admin.route('/admin/authentification') 
def authentification():
    return render_template('admin/authentification.html')