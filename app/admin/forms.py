from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField 
from wtforms.validators import DataRequired, Length, Email

class LoginForm(FlaskForm):
    username = StringField('Nom d\'utilisateur', validators=[DataRequired(), Length(1, 15)])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Gardez la session active') 
    submit = SubmitField('Go!')

