from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField
from wtforms.validators import DataRequired

class EmployeForm(FlaskForm):
    nom = StringField('Nom de l\'employé', validators=[DataRequired()])
    prenom = StringField('Prenom de l\'employé', validators=[DataRequired()])
    matricule = StringField('Matricule de l\'employé', unique=True)
    submit = SubmitField('Submit')