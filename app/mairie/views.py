from flask import render_template, session, redirect, url_for 
from . import mairie
from .forms import EmployeForm
from flask_login import login_required
from .. import db


@mairie.route('/')
@login_required
def accueil():
    return render_template('mairie/accueil.html')

@mairie.route('/presentation') 
def presentation():
    return render_template('mairie/presentation.html')

@mairie.route('/authentification') 
def authentification():
    return render_template('mairie/authentification.html')