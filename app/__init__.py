from flask import Flask, render_template 
from flask_bootstrap import Bootstrap 
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy 
from config import config
from flask_login import LoginManager


bootstrap = Bootstrap()
db = SQLAlchemy()
login_manager = LoginManager()
admin_login_manager = LoginManager()
login_manager.login_view = 'admin.userlogin'
admin_login_manager.login_view = 'admin.adminlogin'


def create_app(config_name):
    app = Flask(__name__) 
    app.config.from_object(config[config_name]) 
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    admin_login_manager.init_app(app)

    from app.mairie import mairie as mairie_blueprint
    from app.admin import admin as admin_blueprint 
    app.register_blueprint(mairie_blueprint)
    app.register_blueprint(admin_blueprint)

    return app